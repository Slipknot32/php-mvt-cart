# Panier en PHP

## Objectif
- Découvrir une architecture MVC.
- Utiliser les session en PHP
- Voir les relations dans les BDD
- Utiliser Un ORM & Moteur de Template

## Contexte
Vous allez devoir créer un panier (de shopping) en PHP avec l'architecture MVC.
Le but est afficher :
- Tous les produits dans la page home, 
- Puis ajouter un/des produit·s dans le panier,
- Calculer le total du panier en €,
- Enregistrer la commande BDD. 

## Intention
- Manipuler des données avec un ORM (Eloquent)
- Afficher la donnée dans la vue avec un moteur de template (Blade)
- Orchestrer vos données avec les controllers 

## Compétence.s concernée.s
- Développer une interface utilisateur web dynamique
- Créer une base de données
- Développer les composants d’accès aux données
- Développer la partie back-end d’une application web ou
web mobile

## Critères de réussite
- Le code est **indenté** et **lisible**.
- Il n’y a pas d’erreurs dans le code.

## Livrable.s
> Dimanche 23h59
- **Lien vers le repo gitlab**.
- **Lien vers la version en ligne**.

## Réalisation attendues
Avant tout choses ! Faite un `composer install`

Puis créer votre Base de donnée à partir du fichier `dump.sql`.

Puis allez modifier les variables le `username` & le `password` dans le fichier `config/database.php`.

Et let's code !

**Bonus** :
Pour les plus téméraire d'entre vous :
- Afficher toutes les commande sur une pages
- Créer un système d'authentification
- Les utilisateurs auront un role (Marchant, Acheteur)

**Contrainte** :
- Faire le moins de php natif possible
- le front est fait avec Bulma
