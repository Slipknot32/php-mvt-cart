@extends('layouts')
@section('content')
<section class="container">
	<h1 class="title">Listes des produits</h1>
	<hr>
	<div class="columns is-desktop">
		{{-- Boucles pour récupérer des produits (https://laravel.com/docs/5.8/blade),
		Bulma : https://bulma.io/documentation/columns/responsiveness/,
		https://bulma.io/documentation/components/card/
		--}}
		
		@foreach ($products as $product)
		<div class="card">
			<div class="card-image">
				<figure class="image is-4by3">
					<img src= "{{ $product->picture }}" alt="Placeholder image">
				</figure>
			</div>
			<div class="card-content">
				<div class="content">
					<p class="title is-4">{{ $product->name }}</p> <br>
					<div class="columns">
						<p> prix : {{ $product->price }}€</p>
					</div>
					<br>
					<div class="columns">
						<form action="/cart/add" method="post">

							<input type="hidden" name="priceForOne" value="{{ $product->price }}" />
							<input type="hidden" name="id" value="{{ $product->id }}" />
							<input type="hidden" name="name" value="{{ $product->name }}" />
							<input class="is-half" type="number" name="quantity" value="1" min="0" />
							<button class="bd-tw-button button is-half">ajouter au panier</button>
							<a href="#" class="bd-tw-button button is-half">voir plus</a>

						</form>
					</div>

				</div>
			</div>
		</div>
		@endforeach
		<textarea name="" id="" cols="30" rows="10"><?php print_r($_SESSION[panier])?></textarea>
	</section>
@endsection