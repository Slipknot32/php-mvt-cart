<?php 

namespace App\Controllers;

use Illuminate\Routing\Redirector;
use App\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Cart;
use App\Models\Order;
use App\Models\User;
/**
 * Controller pour gérer les commande et le panier
 */
class OrderController extends Controller {
	/**
	 * Affiche le résumer de la commande de l'ulisateur si il existe
	 * sinon créer le profile de l'utilisateur
	 * @return view retourne la vue order.create
	 */
	public function create(){
		// return view('order.index');
	}

	/**
	 * On Créer le client,
	 * Enregistre une commande et renvoie vers un récap de la commande
	 * @param  Request $request Récupère les données envoyer par le client
	 * @return redirige vers la récap de la commande
	 * @TIPS : https://laravel.com/docs/5.8/eloquent#inserting-and-updating-models
	 * 				 https://laravel.com/docs/5.8/eloquent-relationships#defining-relationships
	 * 				 pour la redirect:
	 * 				 	$redirect->to("[routeName]"); redirige vers une route
	 * 				  $redirect->back(); redirige vers la route précédente
	 */
	public function store(Request $request, Redirector $redirect){}	
}