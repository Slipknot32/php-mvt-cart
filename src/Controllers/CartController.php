<?php 

namespace App\Controllers;

use Illuminate\Routing\Redirector;
use App\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\Cart;


/**
 * Controller pour gérer les commande et le panier
 */
class CartController extends Controller {

	/**
	 * affiche le panier de l'utilisateur
	 * @param  Cart   $cart Objet pour la panier utilisateur
	 * @return  view retourne la vue order.index
	 */
	public function index(){
		return view('cart.index');
	}

	/**
	 * Ajoutes un produit au panier 
	 * Fait le compte de tout les produits
	 * Calcul le total
	 * @param  Request $request Récupère les requêtes du client
	 * @return view  redirige vers la route pricipale
	 *				 pour la redirect:
	 * 				 	$redirect->to("[routeName]"); redirige vers une route
	 * 				  $redirect->back(); redirige vers la route précédente
	 */
	public function store(Request $request, Redirector $redirect){
		// PRINT_R($request->input('quantity') );

		Cart::total();
		Cart::get();

		return $redirect->back();
	}
}