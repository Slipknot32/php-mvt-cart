<?php 

namespace App\Controllers;

use Illuminate\Routing\Redirector;
use App\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Product;

/**
 * Crontroller pour les produits
 */
class ProductController extends Controller
{
	/**
	 * Affiche les informations pour un seul produit
	 * @param  int  $id      id du product
	 * @return view:Array    retourne les information d'un seul produit
	 *                       dans la view product.show
	 * @TIPS : https://laravel.com/docs/5.8/eloquent#retrieving-single-models
	 */
	public function show($id){}
}