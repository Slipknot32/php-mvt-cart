<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;


// Créer un nouvelle instance de l'ORM 
$capsule = new Capsule;

// Créer la nouvelle connection à la base de donnée
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'kart',
    'username' => 'mylene',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
]);

// Envoie tout les Event de L'orm au container
$capsule->setEventDispatcher(new Dispatcher(new Container));

// Définit tout l'instance en global
$capsule->setAsGlobal();

// Boot Eloquent
$capsule->bootEloquent();